<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmpresasController extends Controller
{
    public function vistaEmpresas(){

    	$resultadosEmpresa = DB::table('empresas')->
    	Select('*')->
    	get();
    	//return $resultadosEmpresa;
    	//$this->registrarEmpresa($resultadosEmpresa);
    	return view('administradores.empresas.verEmpresas', compact('resultadosEmpresa'));
    }


/*
    public function registrarEmpresa(Request $request){

		$nombre_empresa = $request->input('nombre_empresa');
		$direccion = $request->input('direccion');
		$telefono = $request->input('telefono');
		$correo_empresa = $request->input('correo_empresa');
		$responsable = $request->input('responsable');

		$resultados = DB::table('empresas')->insert([
	    'nombre_empresa' => $nombre_empresa,
	    'direccion' => $direccion,
	    'telefono' => $telefono,
	    'correo_empresa' => $correo_empresa,
	    'responsable' => $responsable]);

		//return view('vista_para_admin',compact('resultados'));
    }

    */


    public function registraEmpresa($nombre_empresa, $direccion, $telefono, $correo_empresa, $responsable){

		$resultados = DB::table('empresas')->insert([
	    'nombre_empresa' => $nombre_empresa,
	    'direccion' => $direccion,
	    'telefono' => $telefono,
	    'correo_empresa' => $correo_empresa,
	    'responsable' => $responsable]);

		//return view('vista_para_admin',compact('resultados'));
		echo "Registro exitoso";
    }
}
