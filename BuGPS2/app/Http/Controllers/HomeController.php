<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('vista_para_user');
    }

    public function admin(Request $request){
        return view('vista_para_admin', compact('request'));
    }

    public function profesores(Request $request){
        return view('vista_para_profesores', compact('request'));
    }

    public function usuario(Request $request){
        return view('vista_para_user', compact('request'));
    }


}
