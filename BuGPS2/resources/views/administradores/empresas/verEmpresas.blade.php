@extends('layouts.appAdministradores')

@section('content')

<script src="{{asset('js/administradorEmpresas/crudEmpresas.js')}}"></script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style="text-align: center;">
                    <b style="font-size: large;">{{ __('Dashboard Administrador') }}</b>
                    <br>
                    Bienvenido <b>{{ Auth::user()->nombres }}</b>  {{ __('has iniciado sesión ') }}
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    <div>
                    	<table class="table">
								<tr>
									<th scope="col" style="text-align: center;">Empresa</th>
									<th scope="col" style="text-align: center;">Dirección</th>
									<th scope="col" style="text-align: center;">Teléfono</th>
									<th scope="col" style="text-align: center;">E-mail</th>
									<th scope="col" style="text-align: center;">Contacto Responsable</th>
									<th scope="col" style="text-align: center;"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#user-id-2">Añadir empresa</button></th>
								</tr>



							@foreach($resultadosEmpresa as $datos_Empresa)



							<tr>
								<td style="text-align: center;">{{$datos_Empresa->nombre_empresa}}</td>
								<td style="text-align: center;">{{$datos_Empresa->direccion}}</td>
								<td style="text-align: center;">{{$datos_Empresa->telefono}}</td>
								<td style="text-align: center;">{{$datos_Empresa->correo_empresa}}</td>
								<td style="text-align: center;">{{$datos_Empresa->responsable}}</td>

								<td style="text-align: center;">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#user-id">Editar</button>

								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#user-id" onclick="eliminar({{$datos_Empresa->id_empresa}})">Eliminar</button>
								</td>

							</tr>
							@endforeach
						</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--ooooooooooooooooooooooooooooooooooooooooooooooooooooooo-->

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>




<!--Inicias ventana modal "Eliminar"-->
<div id="user-id" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
      	<labe><b>¿Seguro que desea eliminar la empresa?</b></label>
		<div id="confirmacion" style="text-align: center;">
        </div>

       </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>

<!--Termina ventana modal "Eliminar"-->

<hr>

<div id="user-id-2" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Regitrar una nueva empresa</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

          <div class="form-group">
            <label for="nombre_empresa" class="col-form-label">Nombre empresa:</label>
            <input type="text" class="form-control" id="nombre_empresa" name="nombre_empresa">
            <div id="respuestaNombreEmpresa"></div>
          </div>


         <div class="form-group">
            <label for="direccion" class="col-form-label">Dirección:</label>
            <input type="text" class="form-control" id="direccion" value="" name="direccion">
          </div>

          <div class="form-group">
            <label for="telefono" class="col-form-label">Teléfono:</label>
            <input type="text" class="form-control" id="telefono" value="" name="telefono">
          </div>

          <div class="form-group">
            <label for="correo_empresa" class="col-form-label">Correo Electronico:</label>
            <input type="text" class="form-control" id="correo_empresa" value="" name="correo_empresa">
          </div>

          <div class="form-group">
            <label for="responsable" class="col-form-label">Responable de organizacion:</label>
            <input type="text" class="form-control" id="responsable" value="" name="responsable">
          </div>

			<input type="button" class="btn btn-secondary" name="añadirEmpresa" id="añadirEmpresa" value="Añadir" onclick="registro()">

		<div id="respuesta"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

    	</div>
@endsection



