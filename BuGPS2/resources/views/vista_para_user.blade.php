@extends('layouts.appEstudiantes')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style="text-align: center;">

                <b style="font-size: large;">{{ __('Dashboard Estudiante') }}</b>
                    <br>
                    Bienvenido <b>{{ Auth::user()->nombres }}</b>  {{ __('has iniciado sesión ') }}
            </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Texto') }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
