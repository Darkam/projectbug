<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BuGPS</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {

                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;


            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .fondo{

                }

.container{
  margin: auto;
  background-color: white;
  width: 800px;
  padding: 30px;
}

ul, li {
    padding: 0;
    margin: 0;
    list-style: none;
}

ul.slider{
  position: relative;
  width: 800px;
  height: 300px;
}

ul.slider li {
    position: absolute;
    left: 0px;
    top: 0px;
    opacity: 0;
    width: inherit;
    height: inherit;
    transition: opacity 2s;
    background:#fff;
}

ul.slider li img{
  width: 100%;
  height: 300px;
  object-fit: cover;
}

ul.slider li:first-child {
    opacity: 1; /*Mostramos el primer <li>*/
}

ul.slider li:target {
    opacity: 1; /*Mostramos el <li> del enlace que pulsemos*/
}

.menu{
  text-align: center;
  margin: 20px;
}

.menu li{
  display: inline-block;
  text-align: center;
}

.menu li a{
  display: inline-block;
  color: white;
  text-decoration: none;
  background-color: grey;
  padding: 10px;
  width: 20px;
  height: 20px;
  font-size: 20px;
  border-radius: 100%;
}

        </style>

    </head>
    <body style="background-image: url('/imagenes/'); background-size: cover; ">

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Iniciar sesión</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Registrarse</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <img src="{{asset('/imagenes/Logo2.png')}}" style="height: " >
                </div>



            </div>
            <div class="container">
                  <ul class="slider">
                    <li id="slide1">
                      <img src="https://fotografias.antena3internacional.com/clipping/cmsimages02/2018/09/25/2340813A-74AB-4CA7-AA12-DBA63C49447B/58.jpg" style="height: 300px;" />
                    </li>
                    <li id="slide2">
                      <img src="https://e.rpp-noticias.io/normal/2018/08/24/144114_667419.jpg"/>
                    </li>
                    <li id="slide3">
                      <img src="{{asset('/imagenes/Maps.png')}}"/>
                    </li>
                  </ul>

                  <ul class="menu">
                    <li>
                      <a href="#slide1">1</a>
                    </li>
                    <li>
                      <a href="#slide2">2</a>
                    </li>
                     <li>
                      <a href="#slide3">3</a>
                    </li>
                  </ul>

                </div>
        </div>
    </body>
</html>
