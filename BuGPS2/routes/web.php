<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*------------------------------------------*/

Route::get('/dashboard-admin', 'HomeController@admin')->name('admin.dashboard'); // <--- este es el nombre que busca el controlador.

Route::get('/dashboard-profesores', 'HomeController@profesores')->name('prof.dashboard'); // <--- este es el nombre que busca el controlador.

Route::get('/dashboard', 'HomeController@usuario')->name('regular.dashboard'); // <--- este es el nombre que busca el controlador.

/*Rutas para Admins - empresas*/
Route::get('dashboard/empresas/{nombre_empresa}/{direccion}/{telefono}/{correo_empresa}/{responsable}', 'EmpresasController@registraEmpresa')->name('registraEmpresas');

Route::get('dashboard/empresas', 'EmpresasController@vistaEmpresas')->name('vistaEmpresas');






